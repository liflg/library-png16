Website
=======
http://www.libpng.org/pub/png/libpng.html

License
=======
libpng license (see the file source/LICENSE)

Version
=======
1.6.36

Source
======
libpng-1.6.36.tar.xz (sha256: eceb924c1fa6b79172fdfd008d335f0e59172a86a66481e09d4089df872aa319)

Required by
===========
* SDL2_image
