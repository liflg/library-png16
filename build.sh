#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
        PNG_DEBUG="ON"
        CMAKE_BUILD_TYPE=Debug
    else
        PNG_DEBUG="OFF"
        CMAKE_BUILD_TYPE=Release
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo "Optimization level $OPTIMIZATION is not yet implemented!"
        exit 1
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( cd "$BUILDDIR"
      cmake ../source/ \
         -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
         -DPNG_DEBUG=$PNG_DEBUG \
         -DPNG_SHARED=ON \
         -DPNG_STATIC=OFF \
         -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
         -DPNG_TESTS=OFF
      make -j "$(nproc)"
      make install
      rm -rf "${PREFIXDIR:?}"/{bin,lib/libpng,lib/pkgconfig,share}
      chmod 0755 "$PREFIXDIR"/lib/lib*so* )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/LICENSE "$PREFIXDIR"/lib/LICENSE-png16.txt

rm -rf "$BUILDDIR"

echo "Libpng for $MULTIARCHNAME is ready."
